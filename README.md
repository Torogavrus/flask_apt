#apt_flask

# Quick Start


## Basics

**1. Create project folder and enter inside **

    '''
    mkdir my_project
    cd myproject
    '''

**2. Clone project from Git**

    '''
    git clone https://Torogavrus@bitbucket.org/Torogavrus/flask_apt.git
    '''

**3. Create and activate a virtual environment**

    1. install virtual enviroment
        '''
        pip install virtualenv
        '''

    2. create virtualenv
        **project is on python 3**
        '''
        virtualenv -p python3 my_project_venv
        '''

    3. activate virtualenv
        '''
        source my_project_venv//bin/activate
        '''

    4. update pip and setuptools
        '''
        pip install pip -U
        pip install setuptools -U
        '''

**4. Install the requirements**

    '''
    pip install -r apt_flask/requirements.txt
    '''

**5. Create MySQL data base and configure connection with it**

    1. create database
        in MySQL create database AptDB
        '''
        create database AptDB;
        '''

        NOTE: do not create tables - application create it by itself

    2. in folder apt_flask/project/server rename file config.py.example in to config.py

    3. in this file in line 4 set your username and password to database (replase username and password)

**6. Create tables**

    you have to be in 'apt_falsk' folder - it is root of the project
    1. create tables
        '''
        python manage.py create_tables
        '''
        now you can check that tables are created in MySQL database

    2. Optional: fill tables with test data (test data you can see in manage.py file)
        '''
        python manage.py fill_tables
        '''

    3. Warning: you can drop all tables with command
         '''
         python manage.py drop_tables
         '''
         (but firs uncomment corresponding method in manage.py)

**7. Run the Application**

    '''
    python manage.py runserver
    '''

    and enter in your browser
    http://127.0.0.1:5000/add_json
