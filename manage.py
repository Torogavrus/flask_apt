from datetime import date

from flask_script import Manager

from project.server import app, db
from project.server.models import ZipCodes, Buildings, Listings, Prices

from project.server.service.zip_service import ZipService
from project.server.service.add_json_service import AddJsonService

manager = Manager(app)

@manager.command
def create_tables():
    db.create_all()

# @manager.command
# def drop_tables():
#     db.drop_all()

@manager.command
def fill_tables():
    z1 = ZipCodes(10020, True)
    z2 = ZipCodes('00581', True)
    z3 = ZipCodes('00582')
    db.session.add(z1)
    db.session.add(z2)
    db.session.add(z3)

    b1 = Buildings(z1, 112, 'BroadWay', 'avenue', 'New-York', 'New-York', walk_score=45)
    b2 = Buildings(z2, 12, 'Alstreet', 'road', 'Ancorage', 'Alaska', street_direction='west', walk_score=100)
    b3 = Buildings(z1, 34, 'Central', 'park', 'New-York', 'New-York', street_direction='east', walk_score=24)
    db.session.add(b1)
    db.session.add(b2)
    db.session.add(b3)

    l1 = Listings(b1, 24, bedrooms=2, bathrooms=2, month_available=5, day_available=140, balcony=True, garbage_disposal=True, dishwasher=True, pet_friendly=False, air_conditioning=True)
    l2 = Listings(b2, 1, bedrooms=1, month_available=10, day_available=250,dishwasher=True, pet_friendly=True, stove=True)
    l3 = Listings(b1, 39, bedrooms=1, bathrooms=1, month_available=2, day_available=60, balcony=True, garbage_disposal=True, dishwasher=True, pet_friendly=True)
    db.session.add(l1)
    db.session.add(l2)
    db.session.add(l3)
    db.session.commit()

    p1 = Prices(l1, 200, 10, date(2016, 5, 3))
    p2 = Prices(l2, 65, 25, date(2017, 3, 25))
    p3 = Prices(l1, 100, 25, date(2017, 4, 18))
    db.session.add(p1)
    db.session.add(p2)
    db.session.add(p3)
    db.session.commit()

@manager.command
def add_zips():
    zip_cl = ZipService()
    zip_cl.add_zips_from_file()

@manager.command
def add_js():
    # build_js = {
    #     'zip_code': 10020,
    #     'street_number':98,
    #     'street_direction':'n',
    #     'street_name':'main',
    #     'street_type':'street',
    #     'city':'chicago',
    #     'state':'illinois',
    #     'latitude':41.8781,
    #     'longitude':87.6298,
    #     'walk_score':87,
    #     'unit_number': '1d',
    #     'num_bedrooms': 3,
    #     'num_bathrooms': '1',
    #     'month_available': '5',
    #     'day_available': 120,
    #     'laundry': 1,
    #     'balcony': 0,
    #     'hardwood_floors': 0,
    #     'garbage_disposal': 1,
    #     'dishwasher': 0,
    #     'pet_friendly': 0,
    #     'stove_oven': 0,
    #     'range': 0,
    #     'quoted_price': 150,
    #     'lease_term': 10,
    #     'listing_date': '2017-05-20'
    # }
    build_js = {
        'zip_code': '10582',
        'street_number':10,
        'street_direction':'f',
        'street_name':'lala',
        'street_type':'av',
        'city':'Ancorage',
        'state':'alaska',
        'latitude':57.8781,
        'longitude':98.6298,
        'walk_score':115,
        'unit_number': '15',
        'num_bedrooms': 1,
        'num_bathrooms': '1',
        'month_available': '10',
        'day_available': 200,
        'laundry': 1,
        'balcony': 1,
        'hardwood_floors': 1,
        'garbage_disposal': 1,
        'dishwasher': 0,
        'pet_friendly': 0,
        'stove_oven': 0,
        'range': 0,
        'quoted_price': 80,
        'lease_term': 10,
        'listing_date': '2017-06-18'
    }
    add_js_cl = AddJsonService()
    add_js_cl.add_json(build_js, ignore_whitelist=False)


if __name__ == '__main__':
    manager.run()
