import urllib.request
import urllib.parse

data_json = {
    # 'ignore_whitelist': True,
    'zip_code': '87987',
    'street_number':97,
    'street_direction':'w',
    'street_name':'Wall',
    'street_type':'st',
    'city':'New-York',
    'state':'New-York',
    'latitude':57.8781,
    'longitude':98.6298,
    'walk_score':115,
    'unit_number': '15',
    'num_bedrooms': 1,
    'num_bathrooms': '1',
    'month_available': '10',
    'day_available': 105,
    'laundry': 1,
    'balcony': 1,
    'hardwood_floors': 1,
    'garbage_disposal': 1,
    'dishwasher': 0,
    'pet_friendly': 0,
    'stove_oven': 0,
    'range': 0,
    'quoted_price': 132,
    'lease_term': 95,
    'listing_date': '2017-06-16'
}
form_data = urllib.parse.urlencode(data_json)
form_data = form_data.encode('ascii')
url = 'http://localhost:5000/add_json_data/'
with urllib.request.urlopen(url, data=form_data) as f:
    print(f.read())

