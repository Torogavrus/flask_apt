from flask import request, render_template

from project.server import app

from project.server.service.add_json_service import AddJsonService

@app.route('/add_json/', methods=['GET', 'POST'])
def add_json():
    error = None
    if request.method == 'POST':
        if request.form:
            add_js_cl = AddJsonService()
            ignore_whitelist = False
            if 'ignore_whitelist' in request.form and request.form['ignore_whitelist']:
                ignore_whitelist = True
            added = add_js_cl.add_json(request.form, ignore_whitelist=ignore_whitelist)
            if not added:
                error = 'Invalid Zipcode. We do not currently support your area'
            elif isinstance(added, str):
                error = added
        else:
            error = 'Enter data, please'

    return render_template('add_json.html', error=error)

@app.route('/add_json_data/', methods=['GET', 'POST'])
def add_json_data():
    if request.method == 'POST':
        if request.form:
            add_js_cl = AddJsonService()
            ignore_whitelist = False
            if 'ignore_whitelist' in request.form and request.form['ignore_whitelist']:
                ignore_whitelist = True
            added = add_js_cl.add_json(request.form, ignore_whitelist=ignore_whitelist)
            if not added:
                return 'Invalid Zipcode. We do not currently support your area'
            elif isinstance(added, str):
                return added
            else:
                return 'JSON data added'
    return 'Add json!'