import datetime
from sqlalchemy.dialects.mysql import DATE

from project.server import db


class ZipCodes(db.Model):
    zip_id = db.Column(db.Integer, primary_key=True)
    zip = db.Column(db.String(5), nullable=False, unique=True)
    whitelisted = db.Column(db.Boolean, default=False, nullable=False)

    def __init__(self, zip, whitelisted=False):
        self.zip = str(zip)
        self.whitelisted = whitelisted

    def __repr__(self):
        if self.whitelisted:
            return 'zip code {} in white list'.format(str(self.zip))
        else:
            return 'zip code {} in black list'.format(str(self.zip))


class Buildings(db.Model):
    building_id = db.Column(db.Integer, primary_key=True)

    zip_id = db.Column(db.Integer, db.ForeignKey('zip_codes.zip_id'), nullable=False)
    zipcode = db.relationship('ZipCodes', backref=db.backref('buildings', lazy='dynamic'))

    street_number = db.Column(db.Integer, nullable=False)
    street_name = db.Column(db.String(100), nullable=False)
    street_type = db.Column(db.String(45), nullable=False)
    city = db.Column(db.String(100), nullable=False)
    state = db.Column(db.String(100), nullable=False)

    street_direction = db.Column(db.String(45))
    lat = db.Column(db.Float)
    lng = db.Column(db.Float)
    walk_score = db.Column(db.Integer)

    def __init__(self, zipcode,
                 street_number, street_name, street_type, city, state,
                 street_direction=None, lat=None, lng=None, walk_score=None):
        self.zipcode = zipcode
        self.street_number = int(street_number)
        self.street_name = self.format_string(street_name)
        self.street_type = street_type
        self.city = self.format_string(city)
        self.state = self.format_string(state)

        self.street_direction = street_direction
        self.lat = lat
        self.lng = lng
        self.walk_score = walk_score

    def __repr__(self):
        return 'Building address: {0} {1}/n{2}, {3}, {4}'.format(str(self.street_number),
                                                                 self.street_name, self.city,
                                                                 self.state, self.zipcode.zip)

    def format_string(self, str_data):
        str_data = ' '.join([x.strip() for x in str_data.split() if x.strip()])
        return str_data.title()


class Listings(db.Model):
    listing_id = db.Column(db.Integer, primary_key=True)

    building_id = db.Column(db.Integer, db.ForeignKey('buildings.building_id'), nullable=False)
    building = db.relationship('Buildings', backref=db.backref('listings', lazy='dynamic'))

    unit_number = db.Column(db.String(10), nullable=False)

    bedrooms = db.Column(db.Integer)
    bathrooms = db.Column(db.Integer)
    sqft = db.Column(db.Integer)
    month_available = db.Column(db.Integer)
    day_available = db.Column(db.Integer)
    laundry = db.Column(db.Boolean, default=False)
    balcony = db.Column(db.Boolean, default=False)
    hardwood = db.Column(db.Boolean, default=False)
    garbage_disposal = db.Column(db.Boolean, default=False)
    dishwasher = db.Column(db.Boolean, default=False)
    pet_friendly = db.Column(db.Boolean, default=False)
    air_conditioning = db.Column(db.Boolean, default=False)
    stove = db.Column(db.Boolean, default=False)
    range = db.Column(db.Boolean, default=False)

    def __init__(self, building, unit_number,
                 bedrooms=None, bathrooms=None, sqft=None, month_available=None, day_available=None,
                 laundry=None, balcony=None, hardwood=None, garbage_disposal=None, dishwasher=None,
                 pet_friendly=None, air_conditioning=None, stove=None, range=None):
        self.building = building
        self.unit_number = str(unit_number)

        self.bedrooms = int(bedrooms) if bedrooms else bedrooms
        self.bathrooms = int(bathrooms) if bathrooms else bathrooms
        self.sqft = int(sqft) if sqft else sqft
        self.month_available = int(month_available) if month_available else month_available
        self.day_available = int(day_available) if day_available else day_available
        self.laundry = bool(laundry)
        self.balcony = bool(balcony)
        self.hardwood = bool(hardwood)
        self.garbage_disposal = bool(garbage_disposal)
        self.dishwasher = bool(dishwasher)
        self.pet_friendly = bool(pet_friendly)
        self.air_conditioning = bool(air_conditioning)
        self.stove = bool(stove)
        self.range = bool(range)

    def __repr__(self):
        return 'unit number: {}'.format(self.unit_number)

class Prices(db.Model):
    quote_id = db.Column(db.Integer, primary_key=True)

    listing_id = db.Column(db.Integer, db.ForeignKey('listings.listing_id'), nullable=False)
    listing = db.relationship('Listings', backref=db.backref('prices', lazy='dynamic'))

    quoted_price = db.Column(db.Integer)
    lease_term = db.Column(db.Integer, nullable=False)
    listing_date = db.Column(DATE, nullable=False)

    def __init__(self, listing, lease_term, listing_date, quoted_price=None):
        self.listing = listing
        self.quoted_price = int(quoted_price) if quoted_price else quoted_price
        self.lease_term = int(lease_term)
        self.listing_date = listing_date

    def __repr__(self):
        return 'price {}$'.format(self.quoted_price)
