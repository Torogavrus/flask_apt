from project.server import db
from project.server.models import Listings

class ListingService(object):

    listing_dict = {
        'unit_number': 'unit_number',
        'num_bedrooms': 'bedrooms',
        'num_bathrooms': 'bathrooms',
        'num_sqft': 'sqft',
        'month_available': 'month_available',
        'day_available': 'day_available',
        'laundry': 'laundry',
        'balcony': 'balcony',
        'hardwood_floors': 'hardwood',
        'garbage_disposal': 'garbage_disposal',
        'dishwasher': 'dishwasher',
        'pet_friendly': 'pet_friendly',
        'air_conditioning': 'air_conditioning',
        'stove_oven': 'stove',
        'range': 'range'
    }

    def check_listing_exist(self, init_json, building_exist):
        '''
        check if listing exist
        if it exist - return it
        else create it and return
        :param building_exist, init_json
        :return: Listing object
        '''
        listing_exist = Listings.query.filter_by(
            building=building_exist,
            unit_number=str(init_json['unit_number'])
        ).first()

        if not listing_exist:
            listing_d = {}
            for key, val in self.listing_dict.items():
                if key in init_json and init_json[key]:
                    listing_d[val] = init_json[key]
                elif val in init_json and init_json[val]:
                    listing_d[val] = init_json[val]

            listing_d['building'] = building_exist
            listing = Listings(**listing_d)
            db.session.add(listing)
            db.session.commit()
            return listing
        else:
            return listing_exist
