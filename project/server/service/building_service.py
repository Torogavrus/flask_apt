from project.server import db
from project.server.models import Buildings

class BuildingService(object):

    building_dict = {
        'street_number': 'street_number',
        'street_name': 'street_name',
        'street_type': 'street_type',
        'city': 'city',
        'state': 'state',
        'street_direction': 'street_direction',
        'latitude': 'lat',
        'longitude': 'lng',
        'walk_score': 'walk_score'
    }

    def check_building_exist(self, init_json, zip_exist):
        '''
        check if building exist
        if it exist - return it
        else create it and return
        :param zip_exist, init_json
        :return: Building obj
        '''
        building_exist = Buildings.query.filter_by(
            zipcode=zip_exist,
            street_number=int(init_json['street_number']),
            street_name=init_json['street_name'],
            street_type=init_json['street_type'],
            city=init_json['city'],
            state=init_json['state']
        ).first()

        if not building_exist:
            build_d = {}
            for key, val in self.building_dict.items():
                if key in init_json and init_json[key]:
                    build_d[val] = init_json[key]
                elif val in init_json and init_json[val]:
                    build_d[val] = init_json[val]

            build_d['zipcode'] = zip_exist
            building = Buildings(**build_d)
            db.session.add(building)
            db.session.commit()
            return building
        else:
            return building_exist


