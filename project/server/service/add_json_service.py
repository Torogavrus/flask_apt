from project.server.models import ZipCodes
from project.server.service.zip_service import ZipService
from project.server.service.building_service import BuildingService
from project.server.service.listing_service import ListingService
from project.server.service.price_service import PriceService

class AddJsonService(object):

    obligatory_list = ['zip_code', 'street_number', 'street_name', 'street_type', 'city', 'state', 'unit_number', 'lease_term', 'listing_date']


    def add_json(self, init_json, ignore_whitelist=False):
        if set(self.obligatory_list) < set(init_json.keys()):
            for obl_field in self.obligatory_list:
                if not init_json[obl_field]:
                    return 'Fill all obligatory fields! {} missed.'.format(obl_field)
            zip_exist = ZipCodes.query.filter_by(zip=str(init_json['zip_code'])).first()
            if (zip_exist and zip_exist.whitelisted) or ignore_whitelist:
                if ignore_whitelist:
                    zip_exist = ZipService().check_zip_exist(init_json)
                building_exist = BuildingService().check_building_exist(init_json, zip_exist)
                listing_exist = ListingService().check_listing_exist(init_json, building_exist)
                price_exist = PriceService().check_price_exist(init_json, listing_exist)
                if price_exist == 'Data already exist':
                    return 'Data already exist!'
                return 'Data added'
            else:
                return False
        else:
            return 'Fill all obligatory fields'
