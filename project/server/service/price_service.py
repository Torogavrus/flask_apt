import datetime

from project.server import db
from project.server.models import Prices

class PriceService(object):

    price_dict = {
        'quoted_price': 'quoted_price',
        'lease_term': 'lease_term',
        'listing_date': 'listing_date'
    }

    def check_price_exist(self, init_json, listing_exist):
        '''
        check if price exist
        if it exist - return it
        else create it and return
        :param listing_exist, init_json
        :return: Prices obj
        '''
        listing_date = self.format_date(init_json)

        price_exist = Prices.query.filter_by(
            listing=listing_exist,
            listing_date=listing_date,
            lease_term=init_json['lease_term'],
        ).first()

        if not price_exist:
            price_d = {}
            for key, val in self.price_dict.items():
                if key in init_json and init_json[key]:
                    price_d[val] = init_json[key]
                elif val in init_json and init_json[val]:
                    price_d[val] = init_json[val]

            price_d['listing'] = listing_exist
            price_d['listing_date'] = listing_date
            price = Prices(**price_d)
            db.session.add(price)
            db.session.commit()
            return price
        else:
            return 'Data already exist'

    def format_date(self, init_json):
        listing_date_str = str(init_json['listing_date']).strip()
        for delim in ['-', ' ', ',']:
            d_list = listing_date_str.split(delim)
            if len(d_list) == 3:
                return datetime.date(*[int(x.strip().replace(',', '')) for x in d_list])

        return datetime.datetime.strptime(listing_date_str, '%Y%m%d').date()