from project.server import db
from project.server.models import ZipCodes

class ZipService(object):

    zip_dict = {
        'zip_code': 'zip'
    }

    def add_zips_from_file(self):
        '''
        input data: file 'zip_whitelist.txt' with zipcodes
        all zipcodes from file set to whitelist in database
        all zipcodes that are not file set to blacklist in database
        '''
        # add zip codes to whitelist
        whitelist = []
        with open('zip_whitelist.txt', 'r') as wz:
            for line in wz:
                whitelist.append(line.strip())
                zip_exist = ZipCodes.query.filter_by(zip=line.strip()).first()
                if not zip_exist:
                    zip_code = ZipCodes(line.strip(), True)
                    db.session.add(zip_code)
                elif not zip_exist.whitelisted:
                    zip_exist.whitelisted = True
                    db.session.add(zip_exist)

        db.session.commit()

        # add zip codes to blacklist
        for zip_c in ZipCodes.query.all():
            if zip_c.whitelisted and zip_c.zip not in whitelist:
                zip_c.whitelisted = False
                db.session.add(zip_c)

        db.session.commit()

    def check_zip_exist(self, init_json):
        '''
        check if zip exist
        if it exist - return it
        else create it and return
        :param init_json:
        :return: Zip obj
        '''
        zip_exist = ZipCodes.query.filter_by(
            zip=init_json['zip_code'],
        ).first()

        if not zip_exist:
            zip_d = {}
            for key, val in self.zip_dict.items():
                if key in init_json and init_json[key]:
                    zip_d[val] = init_json[key]
                elif val in init_json and init_json[val]:
                    zip_d[val] = init_json[val]

            zip_d['whitelisted'] = False
            zip_c = ZipCodes(**zip_d)
            db.session.add(zip_c)
            db.session.commit()
            return zip_c
        else:
            return zip_exist